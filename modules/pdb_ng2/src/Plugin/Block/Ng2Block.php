<?php

namespace Drupal\pdb_ng2\Plugin\Block;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\pdb\Plugin\Block\PdbBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Exposes an Angular 2 component as a block.
 *
 * @Block(
 *   id = "ng2_component",
 *   admin_label = @Translation("Angular 2 component"),
 *   deriver = "\Drupal\pdb_ng2\Plugin\Derivative\Ng2BlockDeriver"
 * )
 */
class Ng2Block extends PdbBlock implements ContainerFactoryPluginInterface {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Ng2Block constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid service.
   * @param \Drupal\Core\Extension\ExtensionList $module_list
   *   The module extension list.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UuidInterface $uuid, ExtensionList $module_list) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $uuid);

    $this->moduleExtensionList = $module_list;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uuid'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $info = $this->getComponentInfo();
    $machine_name = $info['machine_name'];

    $build = parent::build();
    $build['#allowed_tags'] = [$machine_name];
    $build['#markup'] = '<' . $machine_name . ' id="instance-id-' . $this->configuration['uuid'] . '"></' . $machine_name . '>';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function attachFramework(array $component) {
    $attached = [];
    $attached['drupalSettings']['pdb']['ng2']['global_injectables'] = [];

    return $attached;
  }

  /**
   * {@inheritdoc}
   */
  public function attachSettings(array $component) {
    $attached = parent::attachSettings($component);

    $machine_name = $component['machine_name'];
    $uuid = $this->configuration['uuid'];

    $attached['drupalSettings']['pdb']['ng2']['components']['instance-id-' . $uuid] = [
      'uri' => $component['path'],
      'element' => $machine_name,
    ];
    // Check if ng_class_name was defined.
    if (!empty($component['ng_class_name'])) {
      // Add "ngClassName" value.
      $attached['drupalSettings']['pdb']['ng2']['components']['instance-id-' . $uuid] += [
        'ngClassName' => $component['ng_class_name'],
      ];
    }
    $attached['drupalSettings']['pdb']['ng2']['module_path'] = $this->moduleExtensionList->getPath('pdb_ng2');

    $config_settings = \Drupal::config('pdb_ng2.settings');
    if (isset($config_settings)) {
      $attached['drupalSettings']['pdb']['ng2']['development_mode'] = $config_settings->get('development_mode');
    }
    else {
      $attached['drupalSettings']['pdb']['ng2']['development_mode'] = TRUE;
    }

    return $attached;
  }

  /**
   * {@inheritdoc}
   */
  public function attachLibraries(array $component) {
    $parent_libraries = parent::attachLibraries($component);

    $framework_libraries = [
      'pdb_ng2/pdb.ng2.config',
    ];

    $libraries = [
      'library' => array_merge($parent_libraries, $framework_libraries),
    ];

    return $libraries;
  }

}
