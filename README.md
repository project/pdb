
# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Developing for PDB
 * Maintainers


## INTRODUCTION

Welcome to Decoupled Blocks. This module is a javascript-framework-agnostic,
progressive decoupling tool to allow custom blocks to be written by front end
developers in the javascript framework of their choice, without needing to know
any Drupal API's.

It keeps individual components encapsulated in their own directories containing
all the css, js, and template assets necessary for them to work, and using an
info.yml file to declare these components and their framework dependencies to
Drupal.


### REQUIREMENTS

There are no special requirements at this time.


### INSTALLATION

 * Enable the module and at least one javascript framework sub-module. The Vue
   submodule is currently the most actively developed and available here:
   [Decoupled Blocks: Vue.js](https://www.drupal.org/project/pdb_vue).
 * For Angular 2 use:
     - Before enabling the Angular 2 sub-module you must navigate to the pdb_ng2
       directory and run `npm install`.
     - Before turning on production module in the module configuration form,
       make sure to run `tsc` from the pdb_ng2 directory to compile your .ts
       files into .js.


### CONFIGURATION

 * Navigate to a block placement UI (core block UI or Panels, for example).

 * Add a block, and select one of the PDB example components.

 * Save your page and the decoupled component will appear!

#### Minimum configuration in component Yaml

```yaml
# Required.
name: My custom component
machine_name: my-custom-component
type: pdb
presentation: ng2   # Name of the submodule like ng2, react, vue, etc.

# Optional.
description: 'I describe the component'
category: 'NG2'     # Category label that appears in block selection form.
version: '1.0.0'    # By default, this is just for informational purposes.
```

#### Attaching JS and CSS via component Yaml

CSS Simplified format:
```yaml
add_css:
  header:
    component:
      'component-name.css': {}
```

Extended format allowing dependencies on other libraries:
```yaml
add_css:
  header:
    css:
      component:
        'component-name.css': {}
    dependencies:
      - pdb/another-component/header
```

JavaScript Simplified format:
```yaml
add_js:
  footer:
    'component-name.js': {}
```

Extended format allowing dependencies on other libraries:
```yaml
add_js:
  footer:
    js:
      'component-name.js': {}
    dependencies:
      - core/drupal.ajax
      - pdb/another-component/footer
```

#### Block Form fields via component Yaml

Declare fields that site editors can fill out when placing a block. This data
will be automatically passed to the browser Window via
`drupalSettings.pdb.configuration` and keyed by the block's UUID.

```yaml
configuration:
  example_field_name:
    type: textfield
    title: 'What do you want to say?'
    default_value: 'Hello world!'
```

#### Entity Context via compenent Yaml

Components can use the context of the main page entity. This data will be
automatically passed to the browser Window via `drupalSettings.pdb.contexts`.

```yaml
contexts:
  entity: node
# context_key: context_value
```

#### Changing Component directory

By default, components will be discovered inside modules and themes. You can
change this to scan other directories outside of modules and themes.

In the `settings.php` file you can define directory paths with the
`pdb_search_dirs` key. This can also be an array of paths.

```php
$settings['pdb_search_dirs'] = DRUPAL_ROOT . '/pdb_components';
```


### DEVELOPING FOR PDB

 * Writing your own components:

   - Writing components for existing frameworks should be straightforward, write
     them as usual and add an info.yml file declaring them to the module. Some
     framework implementations may provide extra tooling or require additional
     boilerplate to use full functionality, see their individual README.md
     files for more information.

   - When you find gaps in functionality you need, post them to the issue queue.

 * Writing your own framework definitions:

   - By extending the main pdb block and deriver classes and adding a library
     file, you can craft your own js framework definition modules! We would love
     to see support for other frameworks.

   - For now you can use the existing pdb_ng2 and pdb_react as examples for
     how to proceed.

 * Options for getting data from Drupal in components

   - If you give your component a required context, it will only appear on pages
     with that context available, and automatically pass that data forward using
     drupalSettings.pdb.contexts. See the ng2_example_node component for sample
     code.

   - If your component requires instance configuration you can add fields in the
     info.yml file that will be displayed on block placement for the site
     builder to fill out. Configuration values are passed to the client via
     drupalSettings.pdb.

   - Use a [JSON:API](https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module)
     endpoint.

   - Use the [GraphQL module](https://www.drupal.org/project/graphql).


### MAINTAINERS

Original Author:
 * Matt Davis (mrjmd) - (https://www.drupal.org/u/mrjmd)

Current maintainers:
 * Mark Miller (segovia94) - (https://www.drupal.org/u/segovia94)
 * Emmanuel Cortes (emacoti) - (https://www.drupal.org/u/emacoti)

Early Contributors:
 * Jason Smith
 * Jeff Lu
 * Shawn Stedman
 * Emma Cortes
 * Diego Barahona
 * Mark Casias
 * Bob Kepford
 * Derek Reese
 * Adam Globus-Hoenich
 * Ted Bowman

This project has been sponsored by:
 * Mediacurrent
   Mediacurrent helps organizations build highly impactful, elegantly designed
   Drupal websites that achieve the strategic results they need. Visit
   (http://www.mediacurrent.com) for more information.

 * Acquia
   Commercially Supported Drupal. Visit (http://acquia.com) for more information.
