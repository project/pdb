<?php

namespace Drupal\pdb\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides an event to handle user custom search dirs.
 */
class PdbDiscoveryEvent extends Event {

  /**
   * Name of the PDB discovery search dirs event.
   */
  const SEARCH_DIRS = 'pdb.search_dirs';

  /**
   * Component discovery search dirs.
   *
   * @var array
   */
  protected $dirs;

  /**
   * PdbDiscoveryEvent constructor.
   *
   * @param array $dirs
   *   The discovery search dirs.
   */
  public function __construct(array $dirs) {
    $this->dirs = $dirs;
  }

  /**
   * Get the dirs.
   *
   * @return array
   *   The stored discovery search dirs.
   */
  public function getDirs() {
    return $this->dirs;
  }

  /**
   * Set the dirs.
   *
   * @param array $dirs
   *   Discovery search dirs to store.
   */
  public function setDirs(array $dirs) {
    return $this->dirs = $dirs;
  }

}
