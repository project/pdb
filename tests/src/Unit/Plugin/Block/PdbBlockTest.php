<?php

namespace Drupal\Tests\pdb\Unit\Plugin\Block;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\pdb\Plugin\Block\PdbBlock;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\pdb\Plugin\Block\PdbBlock
 * @group pdb
 */
class PdbBlockTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Instance of the Plugin.
   *
   * @var \Drupal\pdb\Plugin\Block\PdbBlock
   */
  protected $plugin;

  /**
   * Create the setup for constants and plugin instance.
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock the UUID service.
    $uuid = $this->prophesize(UuidInterface::CLASS);
    $uuid->generate()->willReturn('uuid');
    $uuid_service = $uuid->reveal();

    $context_definition = $this->prophesize(EntityContextDefinition::CLASS);

    $configuration = [
      'pdb_configuration' => [
        'testField' => 'test',
        'second_field' => 1,
        'formatted_field' => [
          'value' => '<p>Formatted text</p>',
        ],
      ],
    ];
    $plugin_id = 'pdb';
    $plugin_definition = [
      'provider' => 'pdb',
      'admin_label' => 'test',
      'info' => [
        'machine_name' => 'example-1',
        'add_js' => [
          'footer' => [
            'example-1.js' => [],
          ],
        ],
        'settings' => [
          'pdb' => ['settings test'],
        ],
        'configuration' => [
          'testField' => [
            'type' => 'textfield',
            'default_value' => 'test',
          ],
          'formatted_field' => [
            'type' => 'text_format',
            'allowed_formats' => [
              'basic_html',
            ],
          ],
        ],
      ],
      'context_definitions' => [
        'entity' => $context_definition->reveal(),
      ],
    ];

    // Create a new instance from the Abstract Class.
    $anonymous_class_from_abstract = new class($configuration, $plugin_id, $plugin_definition, $uuid_service) extends PdbBlock {

      public function returnThis() {
        return $this;
      }

      public function attachFramework(array $component) {
        return ['drupalSettings' => ['pdb' => ['webcomponents' => []]]];
      }

      public function attachPageHeader(array $component) {
        return ['page_attachment'];
      }

      protected function getContextsValues(array $contexts) {
        return ['context_key' => 'context_value'];
      }

      protected function getJsContexts(array $contexts) {
        return ['context_key' => 'context_value'];
      }

      protected function addContextAssignmentElement(ContextAwarePluginInterface $plugin, array $contexts) {
        return 'context';
      }

    };

    $this->plugin = $anonymous_class_from_abstract->returnThis();

    // Create a translation stub for the t() method
    $translator = $this->getStringTranslationStub();
    $this->plugin->setStringTranslation($translator);
  }

  /**
   * Tests the build() method.
   */
  public function testBuild() {
    $expected = [
      '#attached' => [
        'drupalSettings' => [
          'pdb' => [
            'settings test',
            'webcomponents' => [],
            'configuration' => [
              'uuid' => [
                'testField' => 'test',
                'second_field' => 1,
                'formatted_field' => [
                  'value' => '<p>Formatted text</p>',
                ],
              ],
            ],
            'contexts' => [
              'context_key' => 'context_value',
            ],
          ],
        ],
        'pdb/example-1/footer',
        'page_attachment',
      ],
    ];

    $return = $this->plugin->build();
    $this->assertEquals($expected, $return);
  }

  /**
   * Tests the attachLibraries() method.
   *
   * @dataProvider attachLibrariesProvider
   */
  public function testAttachLibraries($value, $expected) {
    $component = [
      'machine_name' => 'example-1',
    ];

    $component = array_merge($component, $value);

    $return = $this->plugin->attachLibraries($component);
    $this->assertEquals($expected, $return);
  }

  /**
   * Provider for testAttachLibraries().
   */
  public static function attachLibrariesProvider() {
    return [
      [
        [
          'add_js' => [
            'header' => [
              'example-1.js' => [],
            ],
          ],
        ],
        [
          'pdb/example-1/header',
        ],
      ],
      [
        [
          'add_css' => [
            'header' => [
              'example-1.css' => [],
            ],
          ],
        ],
        [
          'pdb/example-1/header',
        ],
      ],
      [
        [
          'add_css' => [
            'header' => [
              'example-1.css' => [],
            ],
          ],
          'add_js' => [
            'header' => [
              'example-1.js' => [],
            ],
          ],
        ],
        [
          'pdb/example-1/header',
        ],
      ],
      [
        [
          'add_js' => [
            'footer' => [
              'example-1.js' => [],
            ],
          ],
        ],
        [
          'pdb/example-1/footer',
        ],
      ],
      [
        [
          'add_css' => [
            'footer' => [
              'example-1.css' => [],
            ],
          ],
        ],
        [
          'pdb/example-1/footer',
        ],
      ],
      [
        [
          'add_css' => [
            'footer' => [
              'example-1.css' => [],
            ],
          ],
          'add_js' => [
            'footer' => [
              'example-1.js' => [],
            ],
          ],
        ],
        [
          'pdb/example-1/footer',
        ],
      ],
      [
        [
          'add_css' => [
            'header' => [
              'example-1.css' => [],
            ],
            'footer' => [
              'example-1.css' => [],
            ],
          ],
          'add_js' => [
            'header' => [
              'example-1.js' => [],
            ],
            'footer' => [
              'example-1.js' => [],
            ],
          ],
        ],
        [
          'pdb/example-1/header',
          'pdb/example-1/footer',
        ],
      ],
      [
        [
          'add_css' => [
            'header' => [
              'css' => [
                'example-1.css' => [],
              ],
              'dependencies' => [
                'pdb/example-2/header',
              ],
            ],
            'footer' => [
              'css' => [
                'example-1.css' => [],
              ],
              'dependencies' => [
                'pdb/example-2/footer',
              ],
            ],
          ],
          'add_js' => [
            'header' => [
              'js' => [
                'example-1.js' => [],
              ],
              'dependencies' => [
                'pdb/example-2/header',
              ],
            ],
            'footer' => [
              'js' => [
                'example-1.js' => [],
              ],
              'dependencies' => [
                'pdb/example-2/footer',
              ],
            ],
          ],
        ],
        [
          'pdb/example-1/header',
          'pdb/example-1/footer',
        ],
      ],
    ];
  }

  /**
   * Tests the attachSettings() method.
   */
  public function testAttachSettings() {
    $component = [
      'settings' => [
        'pdb' => ['foobar'],
      ],
    ];

    $expected = [
      'drupalSettings' => [
        'pdb' => ['foobar'],
      ],
    ];

    $return = $this->plugin->attachSettings($component);
    $this->assertEquals($expected, $return);
  }

  /**
   * Tests the buildConfigurationForm() method.
   */
  public function testBuildConfigurationForm() {
    $form_state = $this->createMock(FormStateInterface::CLASS);

    $return = $this->plugin->buildConfigurationForm([], $form_state);
    $this->assertEquals('test', $return['pdb_configuration']['testField']['#default_value']);
    $this->assertEquals('<p>Formatted text</p>', $return['pdb_configuration']['formatted_field']['#default_value']);
  }

}
